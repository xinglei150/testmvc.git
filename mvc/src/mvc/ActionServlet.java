package mvc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.LoginAction;
import action.ZhuceAction;
import unti.FullBean;
import mvc.form.Action;
import mvc.form.ActionForm;
import mvc.form.LoginForm;
import mvc.form.Zhuce;

public class ActionServlet extends HttpServlet {

	public ActionServlet() {
		super();
	}

	public void destroy() {
		super.destroy();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
         //String sign= request.getParameter("sign");
		
		request.setCharacterEncoding("GBK");
		response.setCharacterEncoding("GBK");
		ActionForm form=FullBean.full(request);
		
		Action action=null;
		
		Map<String,String> map=ActionMapping.getMap();
		try{
			String actionname=map.get(request.getParameter("sign"));
			Class clazz=Class.forName(actionname);
			action=(Action)clazz.newInstance();
			
		}
		catch(Exception e){
			
		}
		
		/*if(request.getParameter("sign").equals("mvc.form.LoginForm")){
			action =new LoginAction(); 
		}
		if(request.getParameter("sign").equals("mvc.form.Zhuce")){
			action =new ZhuceAction(); 
		}*/
		String message=action.execute(form);
		PrintWriter out=response.getWriter();
		out.println(message);
		out.flush();
		out.close();
		System.out.println(form);
	}

	 
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			this.doGet(request, response);
	}

	public void init() throws ServletException {
	}

}
