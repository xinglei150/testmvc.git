package unti;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import mvc.form.ActionForm;

public class FullBean {
    public FullBean(){}
    
    public  static ActionForm full(HttpServletRequest request){
    	ActionForm o=null;
    	try{
    		
    		Class clazz=Class.forName(request.getParameter("sign"));
    		o=(ActionForm) clazz.newInstance();
    		Field[] f_ar=clazz.getDeclaredFields();
    		for(Field f:f_ar){
    			f.setAccessible(true);
    			f.set(o, request.getParameter(f.getName()));
    			
    			
    			f.setAccessible(false);
    		}
    	}
    	catch(Exception e){
    		
    	}
    	return o;
    }
}
