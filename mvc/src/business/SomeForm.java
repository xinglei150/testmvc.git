package business;

import mvc.form.ActionForm;

public class SomeForm extends ActionForm implements Comparable{

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
  
	public SomeForm(){}
	
	public String toString(){
		return "a="+this.a+"|| b="+this.b;
	}
	
	private String a="";
	private String b="";
	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getB() {
		return b;
	}

	public void setB(String b) {
		this.b = b;
	}
	
	
}
